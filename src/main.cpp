

#include <Arduino.h>
#include "mylib.h"
#define _debug true
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <ESP8266HTTPClient.h>

#define I_BUTTON0 0

const char *ssid = "SmartHome";    //Ap SSID
const char *password = "12345678"; //Ap Password
HTTPClient httpc;
ESP8266WebServer server(80); //Specify port
long id = 0;
bool wifiAP = false;
void ClearEeprom();
void D_AP_SER_Page();
void Get_Req();
String Essid = "";
String Epass = "";
String sssid = "";
String passs = "";
String token = "";
String Etoken = "";
byte buttonState = 0;
byte lastButtonState = 0;           //the previous reading from the input pin
unsigned long lastDebounceTime = 0; //the last time the output pin was toggled
unsigned long debounceDelay = 50;

void stan()
{
}
void setup()
{

  Serial.begin(9600); //Set Baud Rate
  Serial.println("fddddd");
  EEPROM.begin(512);
  pinMode(I_BUTTON0, INPUT_PULLUP);

  // ClearEeprom();
  //   EEPROM.write(0, false);
  //   // EEPROM.write(132, false);
  //   EEPROM.commit();
  //   delay(1000);

  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);      //Both in Station and Access Point Mode
  for (int i = 1; i < 26; ++i) //Reading SSID
  {
    Essid += char(EEPROM.read(i));
    //Serial.println(Essid);
  }
  for (int i = 26; i < 51; ++i) //Reading Password
  {
    Epass += char(EEPROM.read(i));
    //Serial.println(Epass);
  }
  for (int i = 51; i < 87; ++i) //Reading Password
  {
    Etoken += char(EEPROM.read(i));
    //Serial.println(Etoken);
  }

  if (EEPROM.read(0) == true)
  {
    Serial.println("z eeprom");
    DEBUG(Essid);
    DEBUG(passs);
    WiFi.begin(Essid.c_str(), Epass.c_str());
    server.on("/", stan); //if
    delay(5000);
    //If submit button is pressed get the new SSID and Password and store it in EEPROM
    DEBUG(WiFi.localIP().toString());
  }
  else
  {
    Serial.println("z prog");
    WiFi.softAP(ssid, password);
    server.on("/", D_AP_SER_Page); //if
    //If submit button is pressed get the new SSID and Password and store it in EEPROM
    server.on("/a", Get_Req);
  }
  server.begin();
  delay(5000);
  Serial.println("Działa");
}

//////////////////////////////Reading EEProm SSID-Password////////////////////////////////////////////////////

///////////////////////////////////////////If IP is Hitted in Browser///////////////////////////////////////////
void D_AP_SER_Page()
{
  int Tnetwork = 0;
  String st = "", s = "";
  Tnetwork = WiFi.scanNetworks(); //Scan for total networks available
  st = "<select name='ssid'>";
  for (int i = 0; i < Tnetwork; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<option  value='";
    st += WiFi.SSID(i);
    st += "'>";
    st += i + 1;
    st += ": ";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);
    st += ")";
    st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</option>";
  }
  st += "</select>";

  s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1> Metro Store</h1> ";

  s += "<p>";
  s += "<form method='get' action='a'><label>SSID: </label>";
  s += st;
  s += "<br><label>Paswoord: </label><input name='pass' length=64><br><br><label>Token: </label><input name='token' length=36><input type='submit'></form>";
  s += "</html>\r\n\r\n";

  server.send(200, "text/html", s);
}
///////////////////////////////////////////If IP is Hitted in Browser///////////////////////////////////////////

///////////////////////////////////////////Get SSID & Password//////////////////////////////////////////////////
void Get_Req()
{

  if (server.hasArg("ssid") && server.hasArg("pass") && server.hasArg("token"))
  {
    sssid = server.arg("ssid"); //Get SSID
    passs = server.arg("pass"); //Get Password
    token = server.arg("token");
    DEBUG("1");
  }

  if (sssid.length() > 1 && passs.length() > 1 && token.length() > 1)
  {
    DEBUG("2");
    ClearEeprom(); //First Clear Eeprom
    delay(100);
    for (int i = 0; i < sssid.length(); ++i)
    {
      EEPROM.write(1 + i, sssid[i]);
    }

    for (int i = 0; i < passs.length(); ++i)
    {
      EEPROM.write(26 + i, passs[i]);
    }
    for (int i = 0; i < token.length(); ++i)
    {
      EEPROM.write(51 + i, token[i]);
    }
    // EEPROM.write(137, true);
    EEPROM.write(0, true);
    EEPROM.commit();

    String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Metro Store</h1> ";
    s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
    server.send(200, "text/html", s);
    delay(500);
  }
  String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Metro Store</h1> ";
  s += "<p>ERROR</html>\r\n\r\n";
  server.send(200, "text/html", s);
}
////////////////////////////////////// Get SSID & Password//////////////////////////

//////////////////////////////////////Clear Eeprom/////////////////////////////////
void ClearEeprom()
{
  Serial.println("Clearing Eeprom");
  for (int i = 0; i < 138; ++i)
  {
    EEPROM.write(i, 0);
  }
}
//////////////////////////////////////Clear Eeprom/////////////////////////////////

void loop()
{
  // todo start when wifi not set
  // DEBUG(WiFi.macAddress());
  if (EEPROM.read(0) == true && id == 0)
  {
    DEBUG("bez id");

    String url = "http://mkokos.myqnapcloud.com:3333/api/" + Etoken + "/add/" + WiFi.macAddress() + "/button4";
    DEBUG(url);
    httpc.begin(url); //Specify request destination

    int httpCode = httpc.GET(); //Send the request
    delay(200);
    String httpmsg = httpc.getString();
    Serial.println(httpCode);
    if (httpCode == 200)
    {
      DEBUG("Polączyl sie");
      id = atol(httpmsg.c_str());
      // EEPROM.write(132, true);
      // EEPROMWritelong(133, id);
      // EEPROM.commit();
      DEBUG("dodany");
      delay(5000);
      httpc.end(); //Close connection
    }
    else
    {
      ESP.restart(); /* code */
    }
  }
  else
  {
    if (id != 0)
    {

      byte reading = (digitalRead(I_BUTTON0) == 0 ? 0 : (1 << 0)) ;

      if (reading != lastButtonState)
      {
        lastDebounceTime = millis();
      }
      if ((millis() - lastDebounceTime) > debounceDelay)
      {
        if (reading != buttonState)
        {
          buttonState = reading;
          String value = "0";
          DEBUG("resetdziała??");
          DEBUG(value);
          String link, payload;
          

            if (bitRead(buttonState, 0) == 0)
            {

              value.setCharAt(0, '1');
            }
          
          if (value != "0")
          {
            DEBUG("z id");
            //4 is mininum width, 6 is precision
            DEBUG(value);
            // long id = EEPROMReadlong(133);

            link = "http://mkokos.myqnapcloud.com:3333/api/" + Etoken + "/button4/" + id + "/TOGGLE/" + value;
            // Serial.println("w if"+ id);
            DEBUG(link);
            // Serial.println(link);
            //Close connection
            httpc.begin(link); //Specify request destination

            int httpCode = httpc.GET();  //Send the request
            payload = httpc.getString(); //Get the response payload

            Serial.println(httpCode); //Print HTTP return code
            Serial.println(payload);  //Print request response payload
            httpc.end();
          }
        }
      }

      // Serial.printf("hhhhhhh");
      // delay(2000);
      // delay(2000);

      lastButtonState = reading;
    }
  }
  server.handleClient();
}
